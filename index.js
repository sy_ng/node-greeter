let http = require('http');

let PORT = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World!');
  });
  
  server.listen(PORT, () => {
    console.log(`Server running at port: ${PORT}`);
  });